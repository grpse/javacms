package main;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
/**
 * Cms - Command line reader
 */
public class Cms {

    /*
    * Enter the application
    * */
    public static void main(String[] args) {

        // init input reader and use help to print available commands
        Scanner input = new Scanner(System.in);
        Commander cmder = new Commander();
        cmder.help();
        while(cmder.isRunning()) {
            System.out.print("enter command: ");
            String cmd = input.nextLine();
            // send command to commander class to handle portals and pages creation
            cmder.sendCommand(cmd);
        }
    }
}
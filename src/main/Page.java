package main;

import java.util.*;
/**
 * Page structure
 */
public class Page {

    private static Integer lastID = 0;

    private final Integer id;
    private String title;
    private String subtitle;
    private String description;
    private String main_image_path;
    private String css_path;
    private Integer portal;

    /*
    * Construct - page with id from pages.txt
    * */
    public Page(Integer id) {
        this.id = id;
        if (this.id >= lastID)
            lastID = this.id + 1;

    }

    public Page() {
        this.id = lastID++;
    }

    /*
    * Get title
    * @return title of page
    * */
    public String getTitle() { return title;}

    /*
    * Get Subtitle
    * @return subtitle of page
    * */
    public String getSubtitle() { return subtitle;}

    /*
    * Get Description
    * @return description of page
    * */
    public String getDescription() { return description;}

    /*
    * Get main image path
    * @return main image path
    * */
    public String getMainImagePath() { return main_image_path;}

    /*
    * Get CSS path
    * @return css file path
    * */
    public String getCssPath() { return css_path;}

    /*
    * Get portal
    * @return portal id
    * */
    public Integer getPortal() { return portal;}

    /*
    * Set title
    * @param title - set title
    * */
    public void setTitle(String title) { this.title = title;}

    /*
    * Set subtitle
    * @param subtitle - set subtitle
    * */
    public void setSubtitle(String subtitle) { this.subtitle = subtitle;}

    /*
    * Set description
    * @param description - set description
    * */
    public void setDescription(String description) { this.description = description;}

    /*
    * Set main image
    * @param main image path */
    public void setMainImagePath(String mainImagePath) { this.main_image_path = mainImagePath;}

    /*
    * Set css file path
    * @param ccsPath - set css path
    * */
    public void setCssPath(String cssPath) { this.css_path = cssPath;}

    /*
    * Set portal id
    * @param portalID
    * */
    public void setPortalID(Integer portalID) { this.portal = portalID;}

    /*
    * Get the record that will be appended to pages.txt file
    * @return page data pipe separated
    * */
    public String getRecord() {
        return id + "|" + title + "|" + subtitle + "|" + description + "|" + main_image_path + "|" + css_path + "|" + portal ;
    }

    /*
    * Get schema of pages.txt file
    * @return pages schema
    * */
    public static String getSchema() {
        return "id|title|subtitle|description|main_image_path|css_path|portal";
    }

    /*
    * Parse an entry from pages.txt file
    * @return Page parsed
    * */
    public static Page parsedFromLine(String line) {
        String[] fields = line.split("\\|", -1);
        // set values
        Page page = new Page(Integer.parseInt(fields[0]));
        page.title = fields[1];
        page.subtitle = fields[2];
        page.description = fields[3];
        page.main_image_path = fields[4];
        page.css_path = fields[5];
        page.portal = Integer.parseInt(fields[6]);

        return page;
    }
}
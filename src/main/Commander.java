package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Commander {

    private boolean m_running = true;
    private String m_configFilesPath = ".";
    private List<Portal> m_portals = new ArrayList<Portal>();
    private final String PORTALS_FILE = "portals.txt";
    private final String PAGES_FILE = "pages.txt";
    private HTMLgenerator htmlGenerator = new HTMLgenerator();

    public Commander() {
        loadConfigFromFile();
    }

    /*
    * @return whether or not exit command was executed
    * */
    public boolean isRunning() {
        return m_running;
    }

    /*
    * Set configuration file path and load
    * @param configFilesPath
    * */
    public void loadConfigFileFromPath(String configFilesPath) {
        m_configFilesPath = configFilesPath;
        loadConfigFromFile();
    }

    /*
    * Send a command to try to execute it
    * @param cmd to try to execute
    * */
    public void sendCommand(String cmd) {
        if (cmd.contains(CmdConsts.help)) help();
        else if (cmd.contains(CmdConsts.create_portal)) create_portal();
        else if (cmd.contains(CmdConsts.edit_portal)) edit_portal();
        else if (cmd.contains(CmdConsts.list_portals)) list_portals();
        else if (cmd.contains(CmdConsts.delete_portal)) delete_portal();
        else if (cmd.contains(CmdConsts.create_page)) create_page();
        else if (cmd.contains(CmdConsts.edit_page)) edit_page();
        else if (cmd.contains(CmdConsts.list_pages)) list_pages();
        else if (cmd.contains(CmdConsts.delete_page)) delete_page();
        else if (cmd.contains(CmdConsts.generate_html)) generate_html();
        else if (cmd.contains(CmdConsts.exit)) m_running = false;
        else println("Command not recognized!");
        println("");
        // save new configuration
        saveCurrentToConfigFile();
    }

    /*
    * Print available commands*/
    public void help() {
        println("Available commands:");
        println(CmdConsts.create_portal);
        println(CmdConsts.edit_portal);
        println(CmdConsts.list_portals);
        println(CmdConsts.create_page);
        println(CmdConsts.edit_page);
        println(CmdConsts.list_pages);
        println(CmdConsts.delete_page);
        println(CmdConsts.generate_html);
        println(CmdConsts.help);
        println(CmdConsts.exit);
        println("");
    }

    /*
    * Read and fill portal with information
    * */
    private void create_portal() {
        Scanner input = new Scanner(System.in);
        Portal portal = new Portal();
        println("Enter portal information");

        // create a portal with unique title
        boolean existTitle = true;
        while(existTitle) {
            print("enter title: ");
            String portalTitle = input.nextLine();
            if (findPortalByTitle(portalTitle) != null) {
                println("Portal with title \"" + portalTitle +"\"already exists. Provide another one.");
            }
            else {
                portal.setTitle(portalTitle);
                existTitle = false;
            }
        }

        //verify if the url portal doesn't already exists
        boolean existUrl = true;
        while (existUrl) {
            print("enter url: ");
            String portalUrl = input.nextLine();
            if (findPortalByUrl(portalUrl) != null) {
                println("portal url already exist. Provide another one.");
            }
            else {
                portal.setUrl(portalUrl);
                existUrl = false;
            }
        }

        print("enter main css path: ");
        portal.setCssPath(input.nextLine());
        m_portals.add(portal);
        println("Portal \"" + portal.getTitle() + "\" created!");
    }

    /*
    * Edit portal information
    * */
    private void edit_portal() {

        // find the portal to edit with title provided
        print("enter title of portal to try to find it: ");
        Scanner input = new Scanner(System.in);
        String portal_title = input.nextLine();
        Portal portal = findPortalByTitle(portal_title);

        // if it doesn't exists inform and return
        if (portal == null) {
            println("Portal \"" + portal_title + "\" not found!");
            return;
        }

        println("Enter portal information");
        print("enter title: " + portal.getTitle() + rewindStringLength(portal.getTitle()));
        portal.setTitle(input.nextLine());
        print("enter url: " + portal.getUrl() + rewindStringLength(portal.getUrl()));
        portal.setUrl(input.nextLine());
        print("enter main css path: " + portal.getCssPath() + rewindStringLength(portal.getCssPath()));
        portal.setCssPath(input.nextLine());

        println("Portal \"" + portal.getTitle() + "\" edited!");
    }

    /*
    * Print all portals
    * */
    private void list_portals() {
        if (m_portals.size() == 0) println("No portals created yet!");
        else {
            println("Available portals:");
            for (Portal portal : m_portals) {
                println(portal.getTitle());
            }
        }
    }

    /*
    * Find a portal and delete it
    * */
    private void delete_portal() {
        print("enter title of portal to delete it: ");
        Scanner input = new Scanner(System.in);
        String portal_title = input.nextLine();

        // find portal by title
        Portal portal = findPortalByTitle(portal_title);

        // if it doesn't exists inform and return
        if (portal == null) {
            println("Portal \"" + portal_title + "\" doesn't exists!");
            return;
        }
        m_portals.remove(portal);
        println("Portal \"" + portal_title + "\" deleted!");
    }

    /*
    * Find a portal, read and create page with information
    * */
    private void create_page() {

        // first try find portal to insert this page
        Scanner input = new Scanner(System.in);
        println("Enter page information");
        print("enter title of portal it belongs to: ");
        String portal_title = input.nextLine();
        Portal portal = findPortalByTitle(portal_title);

        // if it doesn't exists inform and return
        if (portal == null) {
            println("Portal \"" + portal_title + "\" not found!");
            return;
        }

        println("Portal \"" + portal.getTitle() + "\" found!");
        println("Enter page information:");
        Page page = new Page();

        // verify if page title doesn't exist in portal
        boolean existPage = true;
        while(existPage) {
            print("enter title: ");
            String pageTitle = input.nextLine();
            if (portal.findPageIndexByTitle(pageTitle) >= 0) {
                println("Page \"" + pageTitle + "\" already exists. Enter another page title.");
            }
            else {
                page.setTitle(pageTitle);
                existPage = false;
            }
        }

        // insert remaining information
        print("enter subtitle: ");
        page.setSubtitle(input.nextLine());
        print("enter description: ");
        page.setDescription(input.nextLine());
        print("enter main image path: ");
        page.setMainImagePath(input.nextLine());
        print("enter main css path: ");
        page.setCssPath(input.nextLine());
        page.setPortalID(portal.getID());
        portal.addPage(page);

        println("Page \"" + page.getTitle() + "\" was created!");
    }

    /*
    * Find a portal and a page inside it to edit the page
    * */
    private void edit_page() {

        // read portal of page title
        Scanner input = new Scanner(System.in);
        println("Enter page information");
        print("enter title of portal it belongs to: ");
        String portal_title = input.nextLine();

        // try to find it
        Portal portal = findPortalByTitle(portal_title);

        // if it was not found, inform and return
        if (portal == null) {
            println("Portal \"" + portal_title + "\" not found!");
            return;
        }

        println("Portal \"" + portal_title + "\" found!");

        // else read page title to try find it
        print("enter page title to edit: ");
        String page_title = input.nextLine();
        Integer page_id = portal.findPageIndexByTitle(page_title);
        // if it was not found, inform and return
        if (page_id < 0) {
            println("Page \"" + page_title + "\" not found!");
            return;
        }

        // else enter page information and set it on right page
        println("Page \"" + page_title + "\" found!");
        println("Enter page information:");
        Page page = portal.getPageByIndex(page_id);
        print("enter title: ");
        page.setTitle(input.nextLine());
        print("enter subtitle: ");
        page.setSubtitle(input.nextLine());
        print("enter description: ");
        page.setDescription(input.nextLine());
        print("enter main image path: ");
        page.setMainImagePath(input.nextLine());
        print("enter main css path: ");
        page.setCssPath(input.nextLine());
        //page.setPortalID(portal.getID());

        println("Page \"" + page.getTitle() + "\" editer!");
    }

    /*
    * Print all pages of a portal
    * */
    private void list_pages() {
        Scanner input = new Scanner(System.in);
        // read title of portal
        print("enter title of portal it belongs to: ");
        String portal_title = input.nextLine();
        Portal portal = findPortalByTitle(portal_title);

        // if it was not found, inform and return
        if (portal == null) {
            println("Portal " + portal_title + " not found!");
            return;
        }

        // print all pages
        portal.list_pages();
    }

    /*
    * Find a portal, a page inside of it and delete
    * */
    private void delete_page() {
        Scanner input = new Scanner(System.in);
        println("Enter portals and page information");
        print("enter title of portal it belongs to: ");

        // try to find portal of page
        String portal_title = input.nextLine();
        Portal portal = findPortalByTitle(portal_title);

        // if it was not found inform and return
        if (portal == null) {
            println("Portal \"" + portal_title + "\" not found!");
            return;
        }

        // else read page title to delete it
        println("Portal \"" + portal_title + "\" found!");
        print("enter page title to delete: ");
        String page_title = input.nextLine();

        // search for the index of page into portal pages array
        Integer page_id = portal.findPageIndexByTitle(page_title);
        // if it was not found, inform and return
        if (page_id < 0) {
            println("Page " + page_title + " not found!");
            return;
        }

        // else remove page returned and inform
        portal.removePage(page_id);
        println("Page \"" + page_title + "\" deleted!");
    }

    /*
    * Generate html to pages and folders
    * */
    private void generate_html() {

        print("enter the output path of generated html: ");
        Scanner input = new Scanner(System.in);
        // get path as string
        String output_path = input.nextLine();
        htmlGenerator.generateWithPortalsAtPath(m_portals, output_path);
    }

    /*
    * Save all portals and pages data to their
    * respectives portals.txt and pages.txt files
    * */
    private void saveCurrentToConfigFile() {

        try {
            BufferedWriter portal_writer = getWriter(getPortalsFilePath());
            BufferedWriter page_writer = getWriter(getPagesFilePath());


            portal_writer.write(Portal.getSchema());
            portal_writer.newLine();
            page_writer.write(Page.getSchema());
            page_writer.newLine();

            for (Portal portal : m_portals) {
                portal_writer.write(portal.getRecord());
                portal_writer.newLine();

                for (Page page : portal.getPages()) {
                    page_writer.write(page.getRecord());
                    page_writer.newLine();
                }
            }

            portal_writer.close();
            page_writer.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * Load configuration data and create portals
    * and pages using containing information
    * */
    private void loadConfigFromFile() {

        File portals_file = new File(getPortalsFilePath());
        File pages_file = new File(getPagesFilePath());

        // dont have a config file to read from
        if (!portals_file.exists() || !pages_file.exists()) return;

        try {
            String line = "";

            // read portals
            // first read header to ignore it
            BufferedReader portal_reader = getReader(getPortalsFilePath());
            line = portal_reader.readLine();
            // read portals
            while ((line = portal_reader.readLine()) != null) {
                Portal portal = Portal.parsedFromLine(line);
                // push portal
                m_portals.add(portal);
            }

            // read pages
            // first read to ignore it
            BufferedReader pages_reader = getReader(getPagesFilePath());
            line = pages_reader.readLine();
            // read pages
            while ((line = pages_reader.readLine()) != null) {
                Page page = Page.parsedFromLine(line);
                // push page
                m_portals.get(page.getPortal()).addPage(page);
            }

            portal_reader.close();
            pages_reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * Create a writer to a file path
    * */
    private BufferedWriter getWriter(String filePath) throws IOException {
        return new BufferedWriter(new FileWriter(filePath));
    }

    /*
    * Create a reader from a file path
    * */
    private BufferedReader getReader(String filePath) throws IOException {
        return new BufferedReader(new FileReader(filePath));
    }

    /*
    * @return portals.txt file path
    * */
    private String getPortalsFilePath() {
        return m_configFilesPath + "/" + PORTALS_FILE;
    }

    /*
    * @return pages.txt file path
    * */
    private String getPagesFilePath() {
        return m_configFilesPath + "/" + PAGES_FILE;
    }

    /*
    * Print wrapper for sysout with line ending
    * */
    private void println(String line) {
        System.out.println(line);
    }

    /*
    * Print wrapper for sysout
    * */
    private void print(String line) {
        System.out.print(line);
    }

    /*
    * Find  portal searching by title
    * */
    private Portal findPortalByTitle(String title) {
        for (Portal portal : m_portals)
            if (portal.getTitle().contains(title))
                return portal;
        return null;
    }

    /*
    * Find  portal searching by url
    * */
    private Portal findPortalByUrl(String url) {
        for (Portal portal : m_portals)
            if (portal.getUrl().contains(url))
                return portal;
        return null;
    }

    /*
    * Create a string containing \b concatenated in same length
    * of str
    * @param string*/
    private String rewindStringLength(String str) {
        String backs = "";
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            backs += "\b";
        }

        return backs;
    }
}

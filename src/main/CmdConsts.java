package main;

/**
 * Manage language of UI and
 */
public class CmdConsts {
    public static final String create_portal = "create portal";
    public static final String edit_portal = "edit portal";
    public static final String list_portals = "list portals";
    public static final String delete_portal = "delete portal";
    public static final String create_page = "create page";
    public static final String edit_page = "edit page";
    public static final String list_pages = "list pages";
    public static final String delete_page = "delete page";
    public static final String generate_html = "generate html";
    public static final String help = "help";
    public static final String exit = "exit";
}

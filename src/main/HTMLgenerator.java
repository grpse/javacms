package main;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;

import javax.swing.text.html.CSS;

public class HTMLgenerator {

    // base path to generate html into
    private String m_path = "";
    // default templates source path
    private String m_portalTemplateFilePath = "resources/portal_template.html";
    private String m_pageTemplateFilePath = "resources/page_template.html";
    // Constants of placeholders that will make the html generate correctly
    private final String MAIN_CSS = "main.css";
    private final String INDEX_HTML = "index.html";
    private final String CSS_PLACEHOLDER = "%CSS_PLACEHOLDER%";
    private final String PAGES_LIST_PLACEHOLDER = "%PAGES_LIST_PLACEHOLDER%";
    private final String TITLE_PLACEHOLDER = "%TITLE_PLACEHOLDER%";
    private final String SUBTITLE_PLACEHOLDER = "%SUBTITLE_PLACEHOLDER%";
    private final String DESCRIPTION_PLACEHOLDER = "%DESCRIPTION_PLACEHOLDER%";
    private final String MAIN_IMAGE_PATH_PLACEHOLDER = "%MAIN_IMAGE_PATH_PLACEHOLDER%";

    // default constructor
    public HTMLgenerator() {

    }

    /*
    * Generate all html of portals and their respective pages into base path
    * provided. If the base path doesn't exists it will be created.
    * @param portals is a list of portals
    * @param path is the path provided as string
    * */
    public void generateWithPortalsAtPath(List<Portal> portals, String path) {

        m_path = path;

        try {
            // verify or create a directory with provided path
            if (tryCreateDir(m_path)) {
                for (Portal portal : portals)
                    generatePortal(portal);
                System.out.println("HTML generated successfully!");
            }
            // if it's not a valid dir, print it and don't do nothing
            else System.out.println("Path provided is not a directory!");
        }
        catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
    * Generate a portal html with portal data
    * @param portal to generate
    * */
    private void generatePortal(Portal portal) throws IOException {

        // get portal template as string
        String portal_template = readFileContent(m_portalTemplateFilePath);

        // create directories for each portal
        String portal_dir = transformIntoFolderName(m_path, portal.getUrl());
        mkdir(portal_dir);

        // replace place holders
        String portal_index = replaceTitle(portal_template, portal.getTitle());
        portal_index = replacePagesList(portal_index, portal.getPagesListHTML());
        portal_index = replaceCSS(portal_index, portal_dir, portal.getCssPath());

        // write portal index page
        writeFileContent(portal_dir + "/" + INDEX_HTML, portal_index);

        // generate all of it's pages
        for (Page page : portal.getPages())
            generatePageInPortalPath(page, portal_dir);
    }

    /*
    * Generate a page html within portal dir path
    * @param page to generate
    * @param portal_dir as base path of portal directory
    * */
    private void generatePageInPortalPath(Page page, String portal_dir) throws IOException {

        // get page template as string
        String page_template = readFileContent(m_pageTemplateFilePath);

        // create page directory
        String page_dir = transformIntoFolderName(portal_dir, page.getTitle());
        mkdir(page_dir);

        // replace place holders
        String page_index = replaceTitle(page_template, page.getTitle());
        page_index = replaceSubtitle(page_index, page.getSubtitle());
        page_index = replaceDescription(page_index, page.getDescription());
        page_index = replaceMainImage(page_index, page_dir, page.getMainImagePath());
        page_index = replaceCSS(page_index, page_dir, page.getCssPath());

        // write page index.html
        writeFileContent(page_dir + "/" + INDEX_HTML, page_index);
    }

    /*
    * Transform a string into a valid name of a directory
    * @param baseDir is the base absolute folder
    * @param str is the string that want to generate a valid path name
    * @return valid absolute path as string
    * */
    private String transformIntoFolderName(String baseDir, String str) {
        return baseDir + "/" + str.toLowerCase().replaceAll("[^a-zA-Z0-9_]", "").replaceAll("\\s+","-");
    }

    /*
    * Replace title on html template
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replaceTitle(String original, String replacement) {
        return original.replaceAll(TITLE_PLACEHOLDER, replacement);
    }

    /*
    * Replace subtitle on html template
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replaceSubtitle(String original, String replacement) {
        return original.replaceAll(SUBTITLE_PLACEHOLDER, replacement);
    }

    /*
    * Replace description on html template
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replaceDescription(String original, String replacement) {
        return original.replaceAll(DESCRIPTION_PLACEHOLDER, replacement);
    }

    /*
    * Replace pages list on html template of portal
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replacePagesList(String original, String replacement) {
        return original.replaceAll(PAGES_LIST_PLACEHOLDER, replacement);
    }

    /*
    * Replace css on html template and copy the css file to destination dir if it exist
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replaceCSS(String original, String destinationDir, String css_path) throws IOException {

        // check if css path indicated exists
        File cssFile = new File(css_path);
        if (cssFile.exists()) {
            copy(css_path, destinationDir + "/" + MAIN_CSS);
            return original.replace(CSS_PLACEHOLDER, MAIN_CSS);
        }
        // use main css equals to portal
        return original.replace(CSS_PLACEHOLDER, "../" + MAIN_CSS);
    }

    /*
    * Replace image path on html template and copy to destination dir if exists
    * @param original template as string
    * @param replacement the string we want to replace
    * @return final html with string replaced with provided replacement
    * */
    private String replaceMainImage(String original, String destinationDir, String main_image_path) throws IOException {

        File imageFile = new File(main_image_path);
        // check if main image exists to make a copy of it
        if (imageFile.exists()) {

            String extension = getFileExtension(main_image_path);
            //String fileName = getFileName(main_image_path);

            // copy main image file to page directory
            String final_filename = "main_image." + extension;
            copy(main_image_path, destinationDir + "/" + final_filename);
            return original.replaceAll(MAIN_IMAGE_PATH_PLACEHOLDER, final_filename);
        }

        return original;
    }

    /*
    * Extract the file extension from a provided file path
    * @param filePath of file
    * @return extension of file
    * */
    private String getFileExtension(String filePath) {
        int extensionIndex = filePath.lastIndexOf('.');
        if (extensionIndex >= 0)
            return filePath.substring(extensionIndex + 1);
        return "";
    }

    /*
    * Extract only the file name without extension from file path
    * @param filePath of file
    * @return file name of provided file path
    * */
    private String getFileName(String filePath) {
        String filePathNormalized = filePath.replaceAll("\\\\", "/");
        int fileNameIndex = filePathNormalized.lastIndexOf('/');
        if (fileNameIndex >= 0) {
            filePathNormalized = filePathNormalized.substring(fileNameIndex + 1);
            String extensionWithDot = "\\." + getFileExtension(filePathNormalized);
            return filePathNormalized.replaceAll(extensionWithDot, "");
        }
        return filePath;
    }

    /*
    * Given a file path read all of it's content and return as a string.
    * If it doesn't exists return an empty string.
    * @param filePath of file to read
    * @return content of file
    * */
    private String readFileContent(String filePath) throws IOException {
        // open the file descriptor
        File file = new File(filePath);
        // if not exists return a empty string
        if (!file.exists()) return "";
        // read all of it's content storing in a buffer
        FileInputStream template_reader = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        template_reader.read(data);
        // close the file and return as UTF-8 encoded string
        template_reader.close();
        return new String(data, "UTF-8");
    }

    /*
    * Given a file path and a content as string write content on destination path
    * @param filePath of destination file
    * @param content of file to write
    * */
    private void writeFileContent(String filePath, String content) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePath), Charset.forName("UTF8"));
        writer.write(content);
        writer.close();
    }

    /*
    * Try to create a directory if it doesn't exists.
    * @param dir_path to try create
    * @return true if the directory is valid dir or it was created and false if it's not a dir
    * */
    private boolean tryCreateDir(String dir_path) throws IOException {

        File filePath = new File(dir_path);
        if (filePath.exists() && !filePath.isDirectory()) return false;
        else if (!filePath.exists()) mkdir(m_path);
        return true;
    }

    /*
    * Classic command to create a directory with path provided
    * @param path of directory to create
    * */
    private void mkdir(String dir_path) throws IOException {
        Files.createDirectories(Paths.get(dir_path));
    }

    /*
    * Copy a file from the origin to detiny path
    * @param originPath of file
    * @param detinyPath of file
    * */
    private void copy(String originPath, String destinyPath) throws IOException {
        Files.copy(Paths.get(originPath), Paths.get(destinyPath));
    }

}

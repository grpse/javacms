package main;

import java.util.*;

/**
 * Portal
 */
public class Portal {

    private static Integer lastID = 0;

    private final Integer id;
    private String title;
    private String url;
    private String css_path;
    private List<Page> pages = new ArrayList<>();

    /*
    * Construct portal with id provided into portals.txt file
    * */
    public Portal(Integer id) {
        this.id = id;
        if (this.id >= lastID)
            lastID = this.id + 1;
    }

    public Portal() {
        this.id = lastID++;
    }

    /*
    * Get portal id
    * @return portal id
    * */
    public Integer getID() { return id;}

    /*
    * Get title
    * @return portal title
    * */
    public String getTitle() { return title;}

    /*
    * Get url
    * @return portal url
    * */
    public String getUrl() { return url;}

    /*
    * Get css file path
    * @return css file path
    * */
    public String getCssPath() { return css_path;}

    /*
    * Set title of portal
    * @param title of portal
    * */
    public void setTitle(String title) { this.title = title;}

    /*
    * Set portal url
    * @param url of portal
    * */
    public void setUrl(String url) { this.url = url;}

    /*
    * Set css file path
    * @param css file path of portal
    * */
    public void setCssPath(String cssPath) { this.css_path = cssPath;}

    /*
    * Add a page to this portal
    * @param Page to portal
    * @return whether or not page was added
    * */
    public boolean addPage(Page page) { return this.pages.add(page);}

    /*
    * Remove page from this portal
    * @param index to remove page
    * @return whether or not remove page
    * */
    public boolean removePage(Integer index) { return this.pages.remove(index);}

    /*
    * Get page with id
    * @param page index from portal
    * @return Page from index
    * */
    public Page getPageByIndex(Integer pageID) { return this.pages.get(pageID);}

    /*
    * Get pages from this portal
    * @return a list of pages
    * */
    public List<Page> getPages() { return pages; }

    /*
    * Print the list of pages
    * */
    public void list_pages() {
        if (pages.size() == 0)
            System.out.println("No pages was create for this portal!");
        else {
            System.out.println("Pages available:");
            for(Page page : pages) {
                System.out.println(page.getTitle());
            }
        }
    }

    /*
    * Find a page index on this portal search for it's title
    * @param title for search
    * @return index of the page on pages array of this portal
    * */
    public Integer findPageIndexByTitle(String title) {
        for (Integer i = 0; i < pages.size(); i++) {
            if (pages.get(i) != null && pages.get(i).getTitle().contains(title)) {
                return i;
            }
        }

        return -1;
    }

    /*
    * Get record line
    * @return line with portal data
    * */
    public String getRecord() {
        return id + "|" + title + "|" + url + "|" + css_path;
    }

    /*
    * Get html list itens with html string
    * @return concatenated list itens as html string
    * */
    public String getPagesListHTML() {
        String html = "";
        for(Page page : pages) {
            String page_url = page.getTitle().toLowerCase().replaceAll("\\s", "-");
            html += "<li>";
            html += "<a href=\"" + page_url + "/index.html\">" + page_url + "</a>";
            html += "</li>";
        }
        return html;
    }

    /*
    * Get portal schema
    * @return schema of portal data
    * */
    public static String getSchema() {
        return "id|title|url|css_path";
    }

    /*
    * Get portal from line data
    * @return line data of portal
    * */
    public static Portal parsedFromLine(String line) {

        String[] fields = line.split("\\|", -1);

        // set values
        Portal portal = new Portal(Integer.parseInt(fields[0]));
        portal.title = fields[1];
        portal.url = fields[2];
        portal.css_path = fields[3];

        return portal;
    }
}